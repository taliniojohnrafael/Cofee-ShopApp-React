
//pages import
import AppNavbar from './components/AppNavbar';
import MyCoffee from './components/MyCoffee';
import Home from './pages/Home';
// import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Page404 from './pages/NotFound';

import Dashboard from './components/AdminDashboard';
// import CourseView from './pages/CourseView';

import {useState, useEffect} from 'react';
import{BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import './App.css';


import {UserProvider} from './UserContext';
import CreateProduct from './components/CreateProduct';

function App() {

//State Hook for the user that will globally accessible using the useContaxt
//This will also be used to storethe user info and be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({id: null, isAdmin:false});

  //function for clearing localstorage
  const unSetUser = () => {
    localStorage.clear();
  }
  
  if(!localStorage.getItem("token") === null){
    useEffect(() => {
      fetch(`http://localhost:4000/users/details`,{
          headers: {
            'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem("token")}`
          }
      }).then(response=> response.json())
        .then(data => {
          setUser({id: data._id, isAdmin: data.isAdmin})
        })
    },[])
  }


  return (
    /*Router/BrowserRouter > Routes > Route*/

    <UserProvider value = {{user, setUser, unSetUser}}>
      <Router>
        <AppNavbar/>
          <Routes>
            {
              (user.isAdmin)
              ?
                <Route path = "/" element = {<Dashboard/>}/>
              :
              <Route path = "/" element = {<Home/>}/>
            }
            <Route path = "/coffee" element = {<MyCoffee/>}/>
            <Route path = "/login" element = {<Login/>}/>
            <Route path = "/register" element = {<Register/>} />
            <Route path = "/createproduct" element = {<CreateProduct/>} />
            <Route path = "/logout" element = {<Logout/>} />
            <Route path = "*" element = {<Page404/>} />

            {/* <Route path = "/courses" element = {<Courses/>}/> */}
            {/* <Route path = "/courses/:courseId" element = {<CourseView/>}/> */}
          </Routes>
      </Router>
    </UserProvider>
    
      
  );
}

export default App;
