import React from 'react';


// create a Context Object
	// is a data type that can be used to store information that can be shared to other componets within the app.
	// context object is a different approach to passing informatin bet. components and allows easier access by the use of prop-drilling.

	// CreateContext is used to create a context react
	const UserContext = React.createContext();

	// the 'Provider' component allows other to consume/use the context object and supplu the necessary information needed to the context Object.
	export const UserProvider = UserContext.Provider;

	export default UserContext;