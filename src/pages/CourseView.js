import {useEffect, useState} from 'react';
import {Container , Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function CourseView(){

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);

    const history = useNavigate();

    // :courseId from Parameter. Declared this const in App.js router
    const {courseId} = useParams();
    console.log(courseId);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_URI}/courses/${courseId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            // const {name, description, price} = data;
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })
    }, [courseId])

    const enroll = (courseId) => {
        fetch(`${process.env.REACT_APP_URI}/users/enroll/${courseId}`, {
            method: 'POST',
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);

            if(data === true){
                Swal.fire({
                    title: 'Succesfully enrolled!',
                    icon: "success",
                    text: "Sure kana ba jan? Wala na 'yang back-outan"
                })
                history("/course");
            }else{
                Swal.fire({
                    title: "Something went wrong!",
                    icon: "error",
                    text: "Dasurv!"
                })
                history("/");
            }
        })
    }

    return(
        <Container>
            <Row>
                <Col lang = '{{span: 6, offset: 3'>
                <Card>
                    <Card.Body className="text-center">
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>PhP {price}</Card.Text>
                        <Card.Subtitle>Class Schedule</Card.Subtitle>
                        <Card.Text>8 am - 5 pm</Card.Text>
                        <Button variant="primary" onClick={() => enroll(
                            courseId)}>Enroll</Button>
                    </Card.Body>	
				</Card>
                </Col>
            </Row>
        </Container>
    )
}