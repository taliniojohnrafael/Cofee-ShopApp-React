import CourseCard from '../components/CourseCard';
import {Container} from 'react-bootstrap';
import { useEffect, useState} from "react";

export default function Courses() {

	const [courses, setCourses] = useState([]);

	// console.log(courseDatas);
	// Syntax: 'for getting the local storage data'
		// localStorage.getItem("propertyName")
	// const local = localStorage.getItem("email");
	// console.log(local)

	useEffect(() => {
		fetch(`${process.env.REACT_APP_URI}/courses/allActiveCourses`,)
		.then(response => response.json())
		.then(data => {
			//console.log()

			// sets the "courses" state to map the data retireced from the fetch request into several "CourseCard" component

			setCourses(data.map(course => {
				return(
					<CourseCard key = {course._id} courseProp = {course}/>
				)
			}))
			
		})
		
	}, [])

	return(
			<Container>
				{courses}
			</Container>
		)
} 

// import CourseCard from '../components/CourseCard';
// import {Container} from 'react-bootstrap';
// import { useEffect, useState} from "react";

// export default function Courses() {

// 	const [courses, setCourses] = useState([]);

// 	useEffect(() => {
// 		fetch(`${process.env.REACT_APP_URI}/products/activeProducts`,)
// 		.then(response => response.json())
// 		.then(data => {

// 			// sets the "courses" state to map the data retireced from the fetch request into several "CourseCard" component

// 			setCourses(data.map(course => {
// 				return(
// 					<CourseCard key = {course._id} courseProp = {course}/>
// 				)
// 			}))
			
// 		})
		
// 	}, [])

// 	return(
// 			<Container>
// 				{courses}
// 			</Container>
// 		)
// } 